mFramework

---

provides usefull modding utils:



- MisDB included (Persistent Table storage)

- useful modules (JSON BASE64 - more to come)
- mCustomEntity
  - CustomActions 
  - Custom Entity RMI's

- Basic ChatCommand AUTH  (Backed by jsonfile in server root)
   - isOwnerPlayer(steamId)
   - isStaffPlayer(steamId) 

- and more...



includes a vscode workspace that provides "most" most added and vanilla functions as intellisense with many Globals predefined.



( most essential methods/functions provided feature vscode intellisense and completion via luadoc)



Intellisense and Code compleation features require the following plugins in vscode:

 - Lua  -- sumneko.lua

 - ErrorLense  -- usernamehw.errorlens





Check out the Workshop Mod: [mFramework Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1942750114)

Check out the GitLab Repo: [mFramework Repo](https://gitlab.com/mismodding/public/mframework/)

Check out the Wiki: [mFramework Wiki](https://gitlab.com/mismodding/public/mframework/-/wikis/home)
