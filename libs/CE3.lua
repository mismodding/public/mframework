--
-- ─── COMMON ─────────────────────────────────────────────────────────────────────
--
---@class vector
---@field x number x-axis
---@field y number y-axis
---@field z number z-axis
--- Vector { x-axis, y-axis, z-axis }
local vector = {x = 0, y = 0, z = 0}

--
-- ─── CRYACTION ──────────────────────────────────────────────────────────────────
--
_G["CryAction"] = {}

---* Are we Running onClient
---@return boolean
function CryAction.IsClient() end

---* Are we Running onServer
---@return boolean
function CryAction.IsDedicatedServer() end

--
-- ─── SYSTEM ─────────────────────────────────────────────────────────────────────
--
_G["System"] = {}
---* Fetch an Entity using its entityId
---@param entityId userdata
---@return entity
function System.GetEntity(entityId) end

---* Fetch an Entity using its Name
---@param name string
---@return entity
function System.GetEntityByName(name) end

---* Returns the Class of an Entity by its entityId
---@param entityId userdata
---@return string
function System.GetEntityClass(entityId) end

---* Fetch All Entities
---@return table
function System.GetEntities() end

---* Fetch All Entities of a Specified Class
---@param class string
---@return table
function System.GetEntitiesByClass(class) end

_G["Script"] = {}

---* Reload a Script Folder
---@param path string
function Script.LoadScriptFolder(path) end

---* Reload a Script
---@param path string
function Script.ReloadScript(path) end

---* call a function to after specified timer
---@param milli number miliseconds, timer
---@param f function function, to run
---@return userdata timerId
function Script.SetTimer(milli,f,...) end

---* set a function to run after specified timer
---@param milli number miliseconds, timer
---@param f function function, to run
---@return userdata timerId
function Script.SetTimerForFunction(milli,f,...) end

---* Use SafeKillTimer(timerId) instead, Kill an Active timer by timerId
---@param timerId userdata
function Script.KillTimer(timerId) end